# Node Testing Image
This is `chez14`'s default docker image for testing Node Projects.

Supported Versions:
- 18: LTS (built every 4 weeks)
- 20: LTS (built every 4 weeks)

Installed Package Manager:
- PNPM

## Usage

### PNPM

```yml
Node Unit Test:
    image: registry.gitlab.com/net.christianto/images/node-testing:20
    stage: test
    before_script:
        - npm config set -- "${CI_API_V4_URL#http*:}/projects/:_authToken" "${CI_JOB_TOKEN}"
        - npm config set -- "${CI_API_V4_URL#http*:}/packages/npm/:_authToken" "${CI_JOB_TOKEN}"
        - npm config set -- "@${CI_PROJECT_ROOT_NAMESPACE}:registry" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/"
        - pnpm install
    script:
        - pnpm run build
```
