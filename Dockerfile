ARG NODE_VERSION=20

FROM node:${NODE_VERSION}

# Install PNPM
ARG PNPM_VERSION=latest-8
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable && \
    corepack prepare pnpm@${PNPM_VERSION} --activate
